# my python3 version https://forum.ixbt.com/topic.cgi?id=8:25651:2338#2338
# of bash script for parallel ffmpeg encoding from https://forum.ixbt.com/topic.cgi?id=8:25651-&ms

import sys,os,math,subprocess,multiprocessing,time
  
input_file='LG\ Hoverboard\ 4K\ Demo.ts'
segments=8
pools=8
  
duration=os.popen(".//ffprobe -i "+input_file+" -show_entries format=duration -v quiet -of csv=p=0").read().split('\n')[0]
  
duration=int(math.ceil(float(duration)))
seg_duration_ms=1000.0*duration/segments
seg_duration=" -t "+str(int(math.ceil(float(duration)))*1.0/segments)
seg_position_ms=0
seg_postition=""
os.system("echo ffconcat version 1.0 > seg_list.ffconcat")
print(seg_duration_ms)
print(seg_duration)
  
#generate PTS
os.system(".//ffmpeg -fflags +genpts -i "+input_file+" -c copy genpts.mkv -y 2> genpts.txt")
run_commands={}
  
for seg in range(segments):
 filename_out="out_"+str(seg)+".mkv"
 os.system("echo file "+filename_out +" >> seg_list.ffconcat")
 filename_log="log_"+str(seg)+".txt"
 if seg==segments-1: seg_duration=""
 run_commands[seg]=".//ffmpeg "+str(seg_postition)+" -i genpts.mkv "+str(seg_duration) \
  +" -c:v libx265 -crf 20 -preset slow " + "-x265-params aq-mode=1:pools=" \
  +str(pools)+" -an "+filename_out+" -y 2> "+filename_log+""
 seg_position_ms=seg_position_ms+seg_duration_ms
 seg_postition=" -ss "+str(seg_position_ms*0.001)
 print(seg_position_ms)
 print(seg_postition)
 print(seg)
  
def task(line):
 print("I'm process " +str(os.getpid()) + ": " + line)
 #os.system(line+"\n")
 p=subprocess.Popen(line, shell=True)
 (output, err) = p.communicate()
 p.wait()
  
start_time = time.time()
with multiprocessing.Pool(processes=segments) as pool:
    pool.map(task, run_commands.values())
  
print("--- %s seconds ---" % (time.time() - start_time))
  
os.system(".//ffmpeg -f concat -i seg_list.ffconcat -i genpts.mkv -map 0:v -map 1:a -c copy output.mkv -y 2> log_merge.txt")
